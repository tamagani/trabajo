"""
WSGI config for trabajo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os, sys

path = '/home/ang/.pyenv/versions/3.8.2/envs/venv38/lib/python3.8/site-packages'

if path not in sys.path:
    sys.path.append(path)


from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'trabajo.settings')

application = get_wsgi_application()
