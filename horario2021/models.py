from django.db import models

import time
import datetime
from django.db import models
from django.utils import timezone
from datetime import date, timedelta
from django.core.validators import MaxValueValidator, MinValueValidator

class Grupo(models.Model):
    """ grupo dentro del departamento al que pertenece una persona  """
    nombre = models.CharField(max_length=30)
    tarea = models.CharField(max_length=40)

    def __str__(self):
        """ String for representing the model object"""
        return self.nombre

class Nivel_salarial(models.Model):
    categories = models.CharField(max_length=2, default='')
    cantidad_hora = models.DecimalField(max_digits=11, decimal_places=9)
    def __str__(self):
        return self.categories

class Tanda(models.Model):
    nombre = models.CharField(max_length=50, default = '')
    importancia = models.PositiveSmallIntegerField('importancia en relación a las otras tandas', default=1, help_text='menor número, primera columna en la tabla que muestra las personas en cada tanda')
    hora_inicio = models.PositiveSmallIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(23)])
    hora_fin = models.PositiveSmallIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(23)])
    def __str__(self):
        return self.nombre

class Personal(models.Model):
    nombre = models.CharField(max_length=50, default = '')
    alias = models.CharField(max_length=50, default = 'correo', unique=True)
    tarea = models.ForeignKey(Grupo, on_delete=models.CASCADE)
    es_responsable = models.BooleanField(default=False)
    responsable = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, limit_choices_to={'es_responsable':True})
    alta = models.DateField('fecha de alta', default=date.today)
    baja = models.DateField('fecha de baja', default=(date(2040,1,10)))
    email = models.EmailField(max_length=254, default = 'soporte@dinahosting.com')
    grupo_salarial = models.ForeignKey(Nivel_salarial, on_delete=models.CASCADE)
    vacas = models.PositiveSmallIntegerField(default=22, validators=[MinValueValidator(0), MaxValueValidator(22)])
    comentarios = models.TextField(default='nada destacable')
    contrato_vigente = models.BooleanField(default=True)
    teletrabajo =  models.BooleanField(default=True)
    def __str__(self):
         return self.alias

class Jornada(models.Model):
    """ definimos las horas que se trabajan -- true -- en un día específico """
    etiqueta =  models.CharField(max_length=50, default = '')
    hora0 = models.BooleanField(default=False)
    hora1 = models.BooleanField(default=False)
    hora2 = models.BooleanField(default=False)
    hora3 = models.BooleanField(default=False)
    hora4 = models.BooleanField(default=False)
    hora5 = models.BooleanField(default=False)
    hora6 = models.BooleanField(default=False)
    hora7 = models.BooleanField(default=False)
    hora8 = models.BooleanField(default=False)
    hora9 = models.BooleanField(default=False)
    hora10 = models.BooleanField(default=False)
    hora11 = models.BooleanField(default=False)
    hora12 = models.BooleanField(default=False)
    hora13 = models.BooleanField(default=False)
    hora14 = models.BooleanField(default=False)
    hora15 = models.BooleanField(default=False)
    hora16 = models.BooleanField(default=False)
    hora17 = models.BooleanField(default=False)
    hora18 = models.BooleanField(default=False)
    hora19 = models.BooleanField(default=False)
    hora20 = models.BooleanField(default=False)
    hora21 = models.BooleanField(default=False)
    hora22 = models.BooleanField(default=False)
    hora23 = models.BooleanField(default=False)
    bonificadas = models.PositiveSmallIntegerField(default=0)
    trabajadas =  models.PositiveSmallIntegerField(default=0)
    trabaja = models.BooleanField(default=False)
    tanda = models.ForeignKey(Tanda, on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.etiqueta


class Turno(models.Model):
    """ para una semana de 8 días, indicas las horas que se trabajan cada día en base a las definidas en Horasdiaria """
    tipo = models.CharField(max_length=50, default = '')
    lunes = models.ForeignKey(Jornada, related_name="lunes",  null= True, on_delete=models.CASCADE)
    martes = models.ForeignKey(Jornada, related_name="martes", null= True, on_delete=models.CASCADE)
    miercoles = models.ForeignKey(Jornada, related_name="miercoles", null= True, on_delete=models.CASCADE)
    jueves = models.ForeignKey(Jornada, related_name="jueves", null= True, on_delete=models.CASCADE)
    viernes = models.ForeignKey(Jornada, related_name="viernes", null= True, on_delete=models.CASCADE)
    sabado = models.ForeignKey(Jornada, related_name="sabado", null= True, on_delete=models.CASCADE)
    domingo = models.ForeignKey(Jornada, related_name="domingo", null= True, on_delete=models.CASCADE)

    def __str__(self):
        return self.tipo


class Festivo(models.Model):
    """festivos en el año, diferenciando si son o no nacioanales ---> afecta al número de personas en turno """
    fecha = models.DateField('día', default=date.today)
    nacional = models.BooleanField(default=False)

    def __str__(self):
        return self.fecha.strftime('%d-%m-%Y')

class Vacacione(models.Model):
	jahr = models.PositiveSmallIntegerField('año', default=2021)
	dvacas = models.PositiveSmallIntegerField('días de vacaciones', default=22)
	libred = models.PositiveSmallIntegerField('días de libre disposición', default=2)
	excesoh	= models.PositiveSmallIntegerField('días por exceso de horas', default=2)  

	def __str__(self):
		return str(self.jahr)

class Hsueltas(models.Model):
    """ horas sueltas para compensar el exceso de horas de los festivos """
    tech = models.ForeignKey(Personal, verbose_name='persona', on_delete=models.CASCADE)
    fecha_festivo = models.DateField('día origen', default=date.today)
    fecha_disfrute = models.DateField('día disfrute', default=date.today)
    horas = models.PositiveSmallIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(10)])

    def __str__(self):
        return str(self.id)


class Solicita_vacas(models.Model):
    tech = models.ForeignKey(Personal, verbose_name='persona', on_delete=models.CASCADE)
    jahr = models.PositiveSmallIntegerField('año', default=2021)
    lunes_1 = models.DateField('primera semana', blank=True, null=True)
    lunes_2 = models.DateField('segunda semana', blank=True, null=True)
    lunes_3 = models.DateField('tercera semana', blank=True, null=True)
    fecha_1 = models.DateField('día 1', blank=True, null=True, default=timezone.now())
    fecha_2 = models.DateField('día 2', blank=True, null=True, default=timezone.now()+timedelta(days=1))
    fecha_3 = models.DateField('día 3', blank=True, null=True, default=timezone.now()+timedelta(days=2))
    fecha_4 = models.DateField('día 4', blank=True, null=True, default=timezone.now()+timedelta(days=3))
    fecha_5 = models.DateField('día 5', blank=True, null=True, default=timezone.now()+timedelta(days=4))
    fecha_6 = models.DateField('día 6', blank=True, null=True, default=timezone.now()+timedelta(days=5))
    fecha_7 = models.DateField('día 7', blank=True, null=True, default=timezone.now()+timedelta(days=6))
    ld_1 = models.DateField('libre disposición 1', blank=True, null=True, default=timezone.now()+timedelta(days=7))
    ld_2 = models.DateField('libre disposición 2', blank=True, null=True, default=timezone.now()+timedelta(days=8))
    ex_1 = models.DateField('exceso horas 1', blank=True, null=True, default=timezone.now()+timedelta(days=9))
    ex_2 = models.DateField('exceso horas 2', blank=True, null=True, default=timezone.now()+timedelta(days=10))
    class Meta:
         constraints = [models.UniqueConstraint(fields=['tech','jahr'], name='holidayPerPersonPerYear')]

class Cuadrante(models.Model):
    """ resgistra el turno de cada técnico cada día """
    tech = models.ForeignKey(Personal, verbose_name='persona', on_delete=models.CASCADE)
    fecha=models.DateField('día', default=date.today)
    turnodia = models.ForeignKey(Jornada, verbose_name='jornada diaria', on_delete=models.CASCADE)
    teletrabajo =  models.BooleanField(default=True)

    class Meta:
        constraints = [models.UniqueConstraint(fields=['tech','fecha'], name='oneShiftperTechperDay')]

    def __str__(self):
        return str(self.id)

