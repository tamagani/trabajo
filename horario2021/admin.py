from django.contrib import admin

from horario2021.models import Personal, Grupo, Jornada, Festivo, Hsueltas, Cuadrante, Turno, Nivel_salarial, Tanda, Vacacione, Solicita_vacas

admin.site.register(Vacacione)

class TandaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'hora_inicio', 'hora_fin')

class CuadranteAdmin(admin.ModelAdmin):
    list_display  = ('fecha', 'tech', 'turnodia')
    list_filter = ['fecha', 'tech']

class PersonalAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'tarea', 'alta', 'grupo_salarial')    
    list_filter= ['tarea','alta','contrato_vigente','teletrabajo','responsable']

class JornadaAdmin(admin.ModelAdmin):
    list_display = ('etiqueta', 'trabajadas', 'bonificadas')
    list_filter = ['bonificadas']

class FestivoAdmin(admin.ModelAdmin):
    list_display = ('fecha', 'nacional')

class TurnoAdmin(admin.ModelAdmin):
    list_display = ('tipo','lunes','martes','miercoles','jueves','viernes','sabado','domingo')

class GrupoAdmin(admin.ModelAdmin):
    list_display = ('nombre','tarea')

class Solicita_vacasAdmin(admin.ModelAdmin): 
    list_display = ('tech',)

class HsueltasAdmin(admin.ModelAdmin):
    list_display = ('tech', 'fecha_festivo','fecha_disfrute', 'horas')

class Nivel_salarialAdmin(admin.ModelAdmin):
    list_display = ('categories','cantidad_hora')

admin.site.register(Cuadrante, CuadranteAdmin)
admin.site.register(Personal, PersonalAdmin)
admin.site.register(Jornada, JornadaAdmin)
admin.site.register(Turno, TurnoAdmin)
admin.site.register(Festivo, FestivoAdmin)
admin.site.register(Grupo, GrupoAdmin)
admin.site.register(Tanda, TandaAdmin)
admin.site.register(Solicita_vacas, Solicita_vacasAdmin)
admin.site.register(Hsueltas,HsueltasAdmin)
admin.site.register(Nivel_salarial, Nivel_salarialAdmin)

