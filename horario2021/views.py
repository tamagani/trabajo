from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.views.generic.edit import UpdateView
import calendar
from datetime import date, datetime, timedelta
from django.db.models import Count, Q

from django.core.exceptions import ObjectDoesNotExist

from django.forms import ModelForm
from django.forms import formset_factory
from django.forms import modelformset_factory 

from django.db import connection
from django.utils import timezone
from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
 
from .models import Cuadrante, Personal, Jornada, Festivo, Hsueltas, Turno, Grupo, Tanda, Vacacione, Solicita_vacas
from .forms import Introducir_semana, Cambio_turnodia, Cambio_diaconsecutivo, Ver_turnos, Solicito_vacaciones


import sys

jahr = 2021


def dia_lunes(dia):
    if dia.weekday()==0:
        lunes = dia
    elif dia.weekday()==1:
        lunes = dia - timedelta(days=1)
    elif dia.weekday()==2:
        lunes = dia - timedelta(days=2)
    elif dia.weekday()==3:
        lunes = dia - timedelta(days=3)
    elif dia.weekday()==4:
        lunes = dia - timedelta(days=4)
    elif dia.weekday()==5:
        lunes = dia - timedelta(days=5)
    elif dia.weekday()==0:
        lunes = dia - timedelta(days=6)
    return lunes





def index(request, anno):
    jahr = anno
    tecs_contrato = Personal.objects.filter(contrato_vigente=True).values_list('alias', flat=True)
    lista_tecs = tecs_contrato.filter(tarea__in=[1,2,3,4,5,6,7,8,9]).values_list('alias', 'id').order_by('alias')

    context = {'lista_tecs':lista_tecs, 'jahr':jahr}
    return render(request, 'horario2021/index_2021.html', context)



def jornadas(request):
    jornada = Jornada.objects.filter(trabaja=True).values_list('etiqueta', 'hora0', 'hora1', 'hora2', 'hora3', 'hora4', 'hora5', 'hora6', 'hora7', 'hora8', 'hora9', 'hora10', 'hora11', 'hora12', 'hora13', 'hora14', 'hora15', 'hora16', 'hora17', 'hora18', 'hora19', 'hora20', 'hora21', 'hora22', 'hora23')
    context = {'jornada':jornada} 
    return render(request, 'horario2021/jornadas.html', context)



def semana(request, anno, dia):
    jahr = anno
    dia_i = date(jahr,1,1)
    dia_fin = date(jahr,12,31)
    dias_lunes,matriz= [],[]
    dicc = {}
    presencial = []
#    enoficina = Personal.objects.filter(contrato_vigente=True).filter(teletrabajo=False).values_list('alias', flat=True)
  
    while dia_i < (dia_fin + timedelta(days=1)):
        if dia_i.weekday() == 0:
            dias_lunes.append(dia_i)
        dia_i += timedelta(days=1)
    
    lunes = dias_lunes[dia-1] 
    dia_semana =  dias_lunes[dia-1]
    grupos = [grupo.nombre for grupo in Grupo.objects.all().order_by('nombre')]   
    tandas = [tanda.nombre for tanda in Tanda.objects.all().order_by('importancia')]

    while dia_semana <(lunes+timedelta(days=7)):
        turnostodos_dia= Cuadrante.objects.filter(fecha=dia_semana)       
        turnos_dia = turnostodos_dia.filter(tech__contrato_vigente=True)
        enpresencial = turnos_dia.filter(turnodia__trabaja=True).filter(teletrabajo=False)
        presencial_dia = [enpresencial.filter(turnodia__tanda__nombre=tandax).count() for tandax in tandas]
        presencial.append(presencial_dia)
        dicc = {}
        for grupo in grupos:
            dicc[grupo]= [[tanda,[]] for tanda in tandas]
        for turno in turnos_dia:
            for grupo in grupos:
                if str(turno.tech.tarea) == grupo:
                    for i in range(len(dicc[grupo])):
                        if str(turno.turnodia.tanda)==dicc[grupo][i][0]:
                            if turno.teletrabajo == False:
                                dicc[grupo][i][1].append('00__'+turno.tech.alias) 
                            else:
                                dicc[grupo][i][1].append(turno.tech.alias)
        info_dia = [dia_semana,dicc]
        dia_semana += timedelta(days=1)
        matriz.append(info_dia)
  

    context = {'matriz':matriz, 'grupos':grupos, 'tandas':tandas, 'presencial':presencial}
    return render(request, 'horario2021/semana.html', context)



def esquema(request, anno, tareas):
    """ muestra para cada día del año cuántas personas de cada tarea en cada turno """
    jahr = anno
    matriz = []
    tandas = [tanda.nombre for tanda in Tanda.objects.all().order_by('importancia')]
    incremento = timedelta(days=1)
    dia = dia_lunes(date(jahr,1,1))
    dia_fin = dia_lunes(date(jahr,12,31))
    personas= [persona.alias for persona in Personal.objects.filter(tarea__nombre=tareas).filter(contrato_vigente=True)]

    while dia <(dia_fin+timedelta(days=7)):
        turnostodos_dia= Cuadrante.objects.filter(fecha=dia)
        turnos_dia = turnostodos_dia.filter(tech__alias__in=personas)
        dicc = {}
        for tanda in tandas:
            dicc[tanda] = 0
        for turno in turnos_dia:
            for tanda in tandas:
                if str(turno.turnodia.tanda)==tanda:
                    dicc[tanda] +=1
        info_dia = [dia,dicc]
        dia += incremento
        matriz.append(info_dia)

    context ={'matriz':matriz, 'tandas':tandas}
    return render(request, 'horario2021/esquema.html', context)




def anual_persona(request, anno, persona='awill'):
    jahr = anno
    dia_inicio = date(jahr,1,1)
    dia_fin = date(jahr,12,31)
    lunes_inicio = dia_lunes(dia_inicio)
    lunes_fin = dia_lunes(dia_fin)+timedelta(days=7)

    turnosanual = Cuadrante.objects.filter(Q(fecha__gte=lunes_inicio),Q(fecha__lt=lunes_fin)).filter(tech=Personal.objects.get(alias=persona)).values('fecha', 'turnodia__etiqueta', 'id').order_by('fecha')
    turnos = []
    for i in range(0,len(turnosanual),7):
        turnos.append(turnosanual[i:i+7])

    """ obtenemos festivos trabajados (horas ) y compensados """
    festivos = [festivo.fecha for festivo in Festivo.objects.filter(fecha__year=jahr)]
    turnos_festivos = Cuadrante.objects.filter(fecha__in=festivos).filter(tech__alias=persona).filter(turnodia__trabaja=True).order_by('fecha')
    turnos_compensados = Cuadrante.objects.filter(Q(fecha__gte=dia_inicio),Q(fecha__lt=lunes_fin)).filter(tech__alias=persona).filter(turnodia__etiqueta='festivo_comp').order_by('fecha')
    festivos_horas = [(dia.fecha,dia.turnodia.trabajadas) for dia in turnos_festivos]
    compensados = [dia.fecha for dia in turnos_compensados]

    context = {'turnos':turnos, 'persona':persona ,'jahr':jahr, 'festivos_horas': festivos_horas, 'compensados':compensados }
    return render(request, 'horario2021/anual_persona.html', context)





def horas(request, anno):
    """ muestra las horas trabajadas por cada persona para cada uno de los doce meses del año """
    jahr = anno
    personas = Personal.objects.filter(contrato_vigente=True).values_list('alias').order_by('alias')
    matriz = []
    for persona in personas:
        horas_persona = [persona[0]]
        horas_anuales = 0
        for i in range(1,13):
            horas_mes = 0
            turnos = Cuadrante.objects.filter(fecha__year=jahr).filter(fecha__month=i).filter(tech=Personal.objects.get(alias=persona[0])).values_list('turnodia__trabajadas').order_by('fecha')
            for turno in turnos:
                horas_mes += turno[0]
            horas_anuales += horas_mes    
            horas_persona.append(horas_mes)
        horas_persona.append(horas_anuales)    
        matriz.append(horas_persona)

    context = {'matriz': matriz}
    return render(request, 'horario2021/horas_trabajadas.html', context)

def dias(request, anno):
    """ muestra los días trabajados por cada persona para cada uno de los doce meses del año """
    jahr = anno
    personas = Personal.objects.filter(contrato_vigente=True).values_list('alias').order_by('alias')
    matriz = []
    for persona in personas:
        dias_persona = [persona[0]]
        dias_anuales = 0
        for i in range(1,13):
            dias_mes = 0
            turnos = Cuadrante.objects.filter(fecha__year=jahr).filter(fecha__month=i).filter(tech=Personal.objects.get(alias=persona[0])).filter(turnodia__trabaja=True).order_by('fecha')
            for turno in turnos:
                dias_mes += 1 
            dias_anuales += dias_mes    
            dias_persona.append(dias_mes)
        dias_persona.append(dias_anuales)    
        matriz.append(dias_persona)

    context = {'matriz': matriz}
    return render(request, 'horario2021/dias_trabajados.html', context)


def bonificadas(request,anno):
    """ muestra las horas bonificadas de cada persona para cada uno de los doce meses del año.
        Separamos las correspondientes a los sábados del resto (noches, festivos, domingos) """
    jahr = anno
    personas = Personal.objects.filter(contrato_vigente=True).order_by('alias')
    festivos = Festivo.objects.filter(fecha__year=jahr).order_by('fecha')
    lista_festivos = [festivo.fecha for festivo in festivos]
    matriz, matriz_sa = [], []
    for persona in personas:
        horas_persona = [persona.alias]
        horas_sa_persona = [persona.alias]
        for i in range(1,13):
            horas_mes = 0
            horas_sa = 0
            horas_bo = 0
            turnos = Cuadrante.objects.filter(fecha__year=jahr).filter(fecha__month=i).filter(tech=Personal.objects.get(alias=persona.alias)).order_by('fecha')
            for turno in turnos:
                if turno.fecha in lista_festivos:
                    horas_mes += turno.turnodia.trabajadas
                if turno.fecha.weekday() == 5:
                    horas_sa += turno.turnodia.trabajadas
                    horas_mes += turno.turnodia.bonificadas
                elif turno.fecha.weekday()==6:
                    horas_mes += turno.turnodia.trabajadas
                else:
                    horas_mes += turno.turnodia.bonificadas
            horas_sa_persona.append(horas_sa)
            horas_persona.append(horas_mes)
        matriz.append(horas_persona)
        matriz_sa.append(horas_sa_persona)

    context = {'matriz': matriz, 'matriz_sa':matriz_sa}
    return render(request, 'horario2021/horas_bonificadas.html', context)


def verturnos(request,anno):
    jahr = anno
    if request.method == 'POST':
        matriz_o = []
        form = Ver_turnos(request.POST)
        grupo = get_object_or_404(Grupo, pk=int(request.POST.get('tarea', '')))
        dia = datetime.strptime(request.POST.get('fecha',''),'%Y-%m-%d').date()
        tag = dia_lunes(dia)
        personas = Personal.objects.filter(contrato_vigente=True).filter(tarea=grupo).order_by('alias')
        for i in range(36):
            turnos = Cuadrante.objects.filter(fecha=tag).filter(tech__alias__in=[persona.alias for persona in personas])
            matriz_o.append([tag,{turno.tech.alias:turno.turnodia.etiqueta for turno in turnos}]) 
            tag += timedelta(days=1)
        matriz = [matriz_o[i:i+7] for i in range(0,35,7)]
        context = {'matriz':matriz}
        return render(request, 'horario2021/ver_turnos.html', context)

    else:
        form = Ver_turnos()
        context = {'form':form}
        return render(request, 'horario2021/cambios_turnos.html', context)



def solicito_vacas(request, anno, persona):
    """ formulario para que cada persona solicite sus vacaciones anuales"""
    vacas = Solicita_vacas.objects.filter(jahr=anno).filter(tech__alias=persona)
    if len(vacas) == 0:
        Solicita_vacas.objects.create(jahr=anno, tech=Personal.objects.get(alias=persona),\
                       lunes_1=dia_lunes(date.today()), \
                       lunes_2= dia_lunes(date.today())+timedelta(days=7), \
                       lunes_3=dia_lunes(date.today())+timedelta(days=14))
    
    vacas = get_object_or_404(Solicita_vacas.objects.filter(jahr=anno).filter(tech__alias=persona))
 
    if request.method == 'POST':
       form = Solicito_vacaciones(request.POST, instance=vacas)
       if form.is_valid():
          solicita_vacas = form.save(commit=True)
          solicita_vacas.save()
          context = {'jahr':anno, 'persona':persona, 'vacas':vacas}
          return render(request, 'horario2021/solicita_vacasok.html', context)  
       else: 
          return render(request, 'horario2021/cambios_turnos.html', {'form':form})
    else:
        
        form = Solicito_vacaciones(instance=vacas)
        context = {'form':form}
        return render(request, 'horario2021/cambios_turnos.html', context)

def vacas_solicitadas(request, anno, grupo):
	personas = Personal.objects.filter(contrato_vigente=True).filter(tarea__nombre=grupo).order_by('alias')
	vacaciones = [{persona:Solicita_vacas.objects.filter(jahr=anno).filter(tech=persona)} for persona in personas]
	dia = dia_lunes(date(jahr,1,1))
	dia_fin = dia_lunes(date(jahr,12,31))
	vacas = {dia+timedelta(days=x):[0] for x in range(0,((dia_fin+timedelta(days=7))-dia).days)}
    # no todas las personas van a tener vacaciones asignadas salvo que se automatice en el alta de la persona  
	for persona in personas:
		try:
			vacas_tech = Solicita_vacas.objects.get(jahr=anno,tech=persona)
			fechas = [vacas_tech.lunes_1+timedelta(days=dia) for dia in range(5) if vacas_tech.lunes_1] + \
			[vacas_tech.lunes_2+timedelta(days=dia) for dia in range(5) if vacas_tech.lunes_2] + \
			[vacas_tech.lunes_3+timedelta(days=dia) for dia in range(5) if vacas_tech.lunes_3] + \
			[vacas_tech.fecha_1]+[vacas_tech.fecha_2]+[vacas_tech.fecha_3]+[vacas_tech.fecha_4] + \
			[vacas_tech.fecha_5]+[vacas_tech.fecha_6]+[vacas_tech.fecha_7] + \
			[vacas_tech.ex_1]+[vacas_tech.ex_2] + [vacas_tech.ld_1]+[vacas_tech.ld_2]

			for i in fechas:
				if not i:
					pass
				else:
					vacas[i] = vacas[i] + [persona.alias]
					vacas[i][0] = len(vacas[i])-1
		except ObjectDoesNotExist:
			pass

	context = {'vacas':vacas}
	return render(request, 'horario2021/vacas_solicitadas.html', context)



        

@login_required()
def importes(request,anno,mese):
    """ cálculo de los importes correspondientes a las horas bonificadas. Debería permitir elegir el mes."""
    jahr = anno
    mes = mese
    matriz = []
    personas = Personal.objects.filter(contrato_vigente=True).order_by('nombre')
    festivos = Festivo.objects.filter(fecha__year=jahr).order_by('fecha')
    lista_festivos = [festivo.fecha for festivo in festivos]
    for persona in personas:
       horas_sa, horas_bo = 0,0
       horas_persona = [persona.nombre]
       horas_persona.append(persona.alias) 
       turnos = Cuadrante.objects.filter(fecha__year=jahr).filter(fecha__month=mes).filter(tech=Personal.objects.get(alias=persona.alias)).order_by('fecha')
       for turno in turnos:
           if turno.fecha in lista_festivos:
               horas_bo += turno.turnodia.trabajadas
           if turno.fecha.weekday() == 5:
               horas_sa += turno.turnodia.trabajadas
               horas_bo += turno.turnodia.bonificadas
           elif turno.fecha.weekday()==6:
               horas_bo += turno.turnodia.trabajadas
           else:
               horas_bo += turno.turnodia.bonificadas
       horas_persona.append(horas_sa)
       horas_persona.append(format(int(horas_sa)*persona.grupo_salarial.cantidad_hora/2,'.2f'))
       horas_persona.append(horas_bo)
       horas_persona.append(format(int(horas_bo)*persona.grupo_salarial.cantidad_hora,'.2f'))

       matriz.append(horas_persona)

    dia = date(jahr,mes,1)

    context = {'matriz':matriz, 'dia':dia}
    return render(request, 'horario2021/importes.html', context)



@login_required()
def cambiosemana(request, anno):
    jahr = anno
    dia_i = date(jahr,1,1)
    dia_fin = date(jahr,12,31)
    dias_lunes,matriz= [],[]
    dicc = {}

    while dia_i < (dia_fin + timedelta(days=1)):
        if dia_i.weekday() == 0:
            dias_lunes.append(dia_i)
        dia_i += timedelta(days=1)

    if request.method == 'POST':
        form = Introducir_semana(request.POST)
        persona = get_object_or_404(Personal, pk=int(request.POST.get('tech', '')))
        turnosemana = get_object_or_404(Turno, pk=int(request.POST.get('turno_semana','')))
        dia = int(request.POST.get('dia',''))
        lunes = dias_lunes[dia]
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=0),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.lunes_id)},)
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=1),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.martes_id)},)
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=2),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.miercoles_id)},)
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=3),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.jueves_id)},)
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=4),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.viernes_id)},)
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=5),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.sabado_id)},)
        Cuadrante.objects.update_or_create(fecha=lunes+timedelta(days=6),tech=Personal.objects.get(alias=persona),defaults={'turnodia':Jornada.objects.get(id=turnosemana.domingo_id)},)
        


        context = {'turno_tech': persona, 'turnosemana': turnosemana, 'jahr':jahr }
        return render(request, 'horario2021/cambiosemanaok.html', context)
    
    else:
        form = Introducir_semana()
        context = {'form':form}
        return render(request, 'horario2021/cambios_turnos.html', context)

@login_required()
def cambiodia(request,anno,dia):
    jahr = anno
    diatech = get_object_or_404(Cuadrante, pk=dia)
    if request.method == 'POST':
        form = Cambio_turnodia(request.POST, instance=diatech)
        if form.is_valid():
            cuadrante = form.save(commit=True)
            cuadrante.save()
            return HttpResponse('Modificación de turno grabada')
        else: 
            return render(request, 'horario2021/cambiodia.html', {'form':form})
    else:
        form = Cambio_turnodia(instance=diatech)
        context = {'form' : form}
    return render(request, 'horario2021/cambiodia.html', context)


@login_required
def cambiodias(request,anno):
    jahr = anno
    if request.method == 'POST':
        form = Cambio_diaconsecutivo(request.POST)  
        persona = get_object_or_404(Personal, pk=int(request.POST.get('tech', '')))
        turno_dia= get_object_or_404(Jornada, pk=int(request.POST.get('turno_dia','')))
        dia_inicio = datetime.strptime(request.POST.get('dia_inicio',''),'%Y-%m-%d').date()
        dia_fin = datetime.strptime(request.POST.get('dia_fin',''),'%Y-%m-%d').date()

        dia= dia_inicio
        while dia < (dia_fin+timedelta(days=1)):
            Cuadrante.objects.update_or_create(fecha=dia,tech=persona, defaults={'turnodia': turno_dia},)
            dia += timedelta(days=1)

        context = {'persona':persona, 'turno_dia':turno_dia, 'jahr':jahr}
        return render(request, 'horario2021/cambio_diasconsecutivosok.html', context)

    else:
        form = Cambio_diaconsecutivo()
        context = {'form':form}
        return render(request, 'horario2021/cambios_turnos.html', context)




@login_required()
def formset_view(request):
    cambiodia_formset = formset_factory(Cambio_turnodia, extra=2)
    formset = cambiodia_formset(request.POST or None)

    # print formset data if it is valid (server logs) 
    if formset.is_valid(): 
        for form in formset: 
            print(form.cleaned_data) 
            cuadrante = form.save(commit=True)
            cuadrante.save()
        return HttpResponse('Modificación de turno grabada')

    context = {'formset':formset}
    return render(request, 'horario2021/intercambiodia.html', context)
   
#@login_required
def vacaciones(request, anno, tareas):
	"""el for se basa en denominaciones añadidas desde el entorno de administración """
	personas = Personal.objects.filter(tarea__nombre=tareas).filter(contrato_vigente=True).order_by('alias')
	vacas_jahr = Vacacione.objects.get(jahr=anno)
	turnos_todos = Cuadrante.objects.filter(fecha__year=jahr).order_by('fecha')
	matriz = []
	for persona in personas:
		i,j,k = 0,0,0
		dicc ={'dvacas':[0 for dia in range(vacas_jahr.dvacas)],'libred':[0 for dia in range(vacas_jahr.libred)],'excesoh':[0 for dia in range(vacas_jahr.excesoh)]}
		turnos = turnos_todos.filter(tech__alias=persona)
		for turno in turnos:
			if str(turno.turnodia.etiqueta) == 'vacas':
				dicc['dvacas'][i] = turno.fecha
				i += 1
			elif str(turno.turnodia.etiqueta) =='libre_disp':
				dicc['libred'][j] = turno.fecha
				j +=1
			elif str(turno.turnodia.etiqueta) == 'exceso_horas':
				dicc['excesoh'][k] = turno.fecha
				k +=1
				
		matriz.append([persona.alias,dicc])
 
	context = {'tarea':tareas, 'matriz':matriz}
	return render(request, 'horario2021/vacaciones_tarea.html', context)

def gestionar_festivos(request, anno, tareas):
	"""el for se basa en denominaciones añadidas desde el entorno de administración """
	personas = Personal.objects.filter(tarea__nombre=tareas).filter(contrato_vigente=True).order_by('alias')
	festivos_todos = Festivo.objects.filter(fecha__year=anno)
	turnos_festivos = Cuadrante.objects.filter(fecha__in=[festivo.fecha for festivo in festivos_todos]).filter(turnodia__trabaja=True).order_by('fecha')
	turnos_compensadas = Cuadrante.objects.filter(fecha__year=2021).filter(tech__alias__in=[persona.alias for persona in personas]).filter(turnodia__etiqueta='festivo_comp').order_by('fecha')
	matriz = []
	for persona in personas:
		trabajadas = [turno.fecha for turno in turnos_festivos.filter(tech__alias=persona)]
		compensadas = [turno.fecha for turno in turnos_compensadas.filter(tech__alias=persona)]
		dicc = {persona:[trabajadas, compensadas]}
		matriz.append(dicc)
 
	context = {'tarea':tareas, 'matriz':matriz}
	return render(request, 'horario2021/gestionar_festivos.html', context)







 
@login_required()
def modelformset_view(request):
    """ muestra todos los registros existentes  """
    cambiodia_formset = modelformset_factory(Cuadrante, fields=['fecha', 'tech', 'turnodia'], max_num=2)
    formset = cambiodia_formset(request.POST or None)

    # print formset data if it is valid (server logs) 
    if formset.is_valid(): 
        for form in formset: 
            print(form.cleaned_data) 

    context = {'formset':formset}
    return render(request, 'horario2021/intercambiodia.html', context)



























