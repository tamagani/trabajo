from django.urls import path
from . import views


app_name= 'horario2021'

urlpatterns  = [
    path('<int:anno>/', views.index, name='index'),
    path('<int:anno>/cambiosemana', views.cambiosemana, name='cambiosemana'),
    path('<int:anno>/cambiodia/<int:dia>/', views.cambiodia, name='cambiodia'),
    path('<int:anno>/cambiodias/', views.cambiodias, name='cambiodias'),
    path('<int:anno>/intercambiodia0/', views.modelformset_view, name='modelformset_view'),
    path('<int:anno>/intercambiodia/', views.formset_view, name='formset_view'),
    path('jornadas/', views.jornadas, name='jornadas'),
    path('<int:anno>/semana/<int:dia>/', views.semana, name='semana'),
    path('<int:anno>/esquema/<str:tareas>/', views.esquema, name='esquema'),
    path('<int:anno>/anual/<str:persona>/', views.anual_persona, name='anual_persona'),
    path('<int:anno>/horas/', views.horas, name='horas'),
    path('<int:anno>/dias/', views.dias, name='dias'),
    path('<int:anno>/bonificadas/', views.bonificadas, name='bonificadas'),
    path('<int:anno>/importes/<int:mese>', views.importes, name='importes'),
    path('<int:anno>/vacaciones/<str:tareas>/',views.vacaciones, name='vacaciones'),
    path('<int:anno>/ver_turnos/', views.verturnos, name='ver_turnos'),
    path('<int:anno>/gestionar_festivos/<str:tareas>/',views.gestionar_festivos, name='gestionar_festivos'),
    path('<int:anno>/vacas_solicitadas/<str:grupo>/', views.vacas_solicitadas, name='vacas_solicitadas'),
           ]
