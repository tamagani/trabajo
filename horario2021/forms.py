from django import forms
from django.forms import SelectDateWidget, ModelForm, DateInput, NumberInput

from .models import Personal, Turno, Cuadrante, Jornada, Grupo, Solicita_vacas

from datetime import date, datetime, timedelta 
from django.contrib.admin import widgets



jahr = 2021
dia_i = date(jahr,1,1)
dia_fin = date(jahr,12,31)
dias_lunes = []
i = 0
while dia_i < (dia_fin + timedelta(days=1)):
    if dia_i.weekday() == 0:
        dias_lunes.append((i,dia_i))
        i+=1
    dia_i += timedelta(days=1)



class Introducir_semana(forms.Form):
    tech = forms.ModelChoiceField(queryset = Personal.objects.filter(contrato_vigente = True).order_by('alias'))
    dia = forms.ChoiceField(choices=dias_lunes)
    turno_semana = forms.ModelChoiceField(queryset = Turno.objects.all().order_by('tipo'))




class Cambio_turnodia(forms.ModelForm):

    class Meta:
        model = Cuadrante
        fields = ('fecha', 'tech', 'turnodia', 'teletrabajo')

    def clean(self):
        super(Cambio_turnodia, self).clean()

        turnodia = self.cleaned_data.get('turnodia')
        # si es un turno de noche  afecta al día siguiente
        if str(turnodia)=='noche':
            pass 

        return self.cleaned_data



class Cambio_diaconsecutivo(forms.Form):
    	
    tech = forms.ModelChoiceField(queryset = Personal.objects.filter(contrato_vigente = True).order_by('alias'))
    turno_dia = forms.ModelChoiceField(queryset = Jornada.objects.all().order_by('etiqueta'))
    dia_inicio = forms.DateField(widget=NumberInput(attrs={'type': 'date'}))
    dia_fin = forms.DateField(widget=NumberInput(attrs={'type': 'date'})) 

class Solicito_vacaciones(forms.ModelForm):

    class Meta:
        model = Solicita_vacas 
        exclude = ['tech', 'jahr']
        widgets = {
            'lunes_1': NumberInput(attrs={'type': 'date'}),
            'lunes_2': NumberInput(attrs={'type': 'date'}),
            'lunes_3': NumberInput(attrs={'type': 'date'}),
            'fecha_1': NumberInput(attrs={'type': 'date'}),
            'fecha_2': NumberInput(attrs={'type': 'date'}),
            'fecha_3': NumberInput(attrs={'type': 'date'}),
            'fecha_4': NumberInput(attrs={'type': 'date'}),
            'fecha_5': NumberInput(attrs={'type': 'date'}),
            'fecha_6': NumberInput(attrs={'type': 'date'}),
            'fecha_7': NumberInput(attrs={'type': 'date'}),
            'ld_1': NumberInput(attrs={'type': 'date'}),
            'ld_2': NumberInput(attrs={'type': 'date'}),
            'ex_1': NumberInput(attrs={'type': 'date'}),
            'ex_2': NumberInput(attrs={'type': 'date'}),

}


class Ver_turnos(forms.Form):

    tarea = forms.ModelChoiceField(queryset = Grupo.objects.all().order_by('nombre'))
    fecha = forms.DateField(widget=NumberInput(attrs={'type': 'date'}))
